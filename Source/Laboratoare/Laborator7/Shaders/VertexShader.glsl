#version 330

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_texture_coord;

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;
uniform float Time;
uniform int Hour;

// Uniforms for light properties
uniform vec3 light_position;
uniform vec3 eye_position;
uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;

// Output value to fragment shader
out vec3 color;

void main()
{
	// TODO: compute world space vectors
	vec3 world_pos = (Model * vec4(v_position,1)).xyz;
	vec3 world_normal = normalize( mat3(Model) * v_normal );
	vec3 N = normalize(v_normal);
	vec3 L = normalize( light_position - world_pos );
	vec3 V = normalize( eye_position - world_pos );
	vec3 H = normalize( L + V );
	float culoare_lumina = 1;

	// TODO: define ambient light component
	float ambient_light = material_kd * 0.25;

	// TODO: compute diffuse light component
	float diffuse_light = material_kd * (culoare_lumina) * clamp(dot(N,L), 0, 1);

	// TODO: compute specular light component
	int primeste_lumina = 0;
	if (dot(N,  L) > 0) {
		primeste_lumina = 1;
	}
	
	float specular_light = material_ks * culoare_lumina *  primeste_lumina * pow(clamp(dot(N, H), 0, 1), material_shininess);

	if (diffuse_light <= 0)
	{
		specular_light = 0;
	}

	// TODO: compute light


	// TODO: send color light output to fragment shader
	float d = distance(light_position, world_pos);
	float factor_atenuare = 1 / (d*d);
	float culoareObiect = ambient_light + factor_atenuare * ( diffuse_light + specular_light);
	
	//color = object_color;
	if (0 <= Hour && Hour < 6 || 22 <= Hour && Hour < 24) {
		//culoareObiect += (Time - 6) / 8;
	}
	else if (6 <= Hour && Hour < 14) {
		culoareObiect +=  (Time - 6) / 8;
	}
	else if (14 <= Hour && Hour < 22) {
		culoareObiect += (1 - (Time - 14) / 8);
	}

	color = object_color * vec3(culoareObiect, culoareObiect, culoareObiect);

	if (18 <= Hour && Hour < 20) {
		color.x += (Time - 18) / 255 * 30;
		color.y -= (Time - 18) / 255 * 15;
		color.z -= (Time - 18) / 255 * 15;
	}
	if (20 <= Hour && Hour < 22) {
		color.x += (22 - Time) / 255 * 22;
		color.y -= (22 - Time) / 255 * 15;
		color.z -= (22 - Time) / 255 * 15;
	}
	gl_Position = Projection * View * Model * vec4(v_position, 1.0);
}
