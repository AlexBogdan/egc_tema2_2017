#pragma once
#include <Component/SimpleScene.h>
#include <Component/Transform/Transform.h>
#include <Core/GPU/Mesh.h>

class Laborator7 : public SimpleScene
{
	public:
		Laborator7();
		~Laborator7();

		void Init() override;

	private:
		void FrameStart() override;
		glm::vec2 autostrada_inainte(glm::vec2 start, int portiuni);
		glm::vec2 autostrada_stanga(glm::vec2 start, int portiuni);
		glm::vec2 autostrada_dreapta(glm::vec2 start, int portiuni);
		void Laborator7::reset_player();
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 &modelMatrix, const glm::vec3 &color = glm::vec3(1));

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

		glm::vec3 lightPosition;
		unsigned int materialShininess;
		float materialKd;
		float materialKs;

		glm::vec3 pozitie_autobuz;
		float viteza_autobuz;
		float unghi_autobuz = 0;

		glm::vec3 roata_fata_stanga = glm::vec3(0.075, 0.5 -0.017, 0.225);
		glm::vec3 roata_fata_dreapta = glm::vec3(-0.14, 0.5 -0.017, 0.225);
		glm::vec3 roata_spate_stanga = glm::vec3(0.075, 0.5 -0.017, -0.125);
		glm::vec3 roata_spate_dreapta = glm::vec3(-0.14, 0.5 -0.017, -0.125);

		//glm::mat4 projectionMatrix;
		glm::vec3 pozitie_camera = glm::vec3(0, 1.5, 0);
		float unghi_camera = 0;
		int camera_type = 0; // 0 = free / 1 = third / 2 = first

		int lives = 3;
		bool end_game = false;

		GLenum polygonMode = GL_FILL;
};
