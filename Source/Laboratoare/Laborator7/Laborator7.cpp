#include "Laborator7.h"

#include <vector>
#include <string>
#include <iostream>

#include <Core/Engine.h>

using namespace std;

Laborator7::Laborator7()
{
}

Laborator7::~Laborator7()
{
}

vector< pair<int, int> > autostrada;
vector< pair<int, glm::vec3> > obstacole; // <Tip obstacol, pozitie obstacol>
void Laborator7::Init()
{
	// Citim datele legate de autostrada (pana la 0 0)
	{
		int directie = -1, numar_portiuni = -1;
		while (cin >> directie && cin >> numar_portiuni) {
			if (directie == 0 && numar_portiuni == 0) {
				break;
			}
			autostrada.push_back({directie, numar_portiuni});
		}
	}

	//Rotim Camera catre scena
	GetSceneCamera()->SetRotation(glm::quat(RADIANS(0), glm::vec3(0, 1, 0)));

	{
		Mesh* mesh = new Mesh("box");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("sphere");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("plane");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "plane50.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("bus");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "bus.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("wheel");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "wheel3.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("plane");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "plane50.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// Create a shader program for drawing face polygon with the color of the normal
	{
		Shader *shader = new Shader("ShaderLab7");
		shader->AddShader("Source/Laboratoare/Laborator7/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Laborator7/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	//Light & material properties
	{
		lightPosition = glm::vec3(0, 1, 1);
		materialShininess = 30;
		materialKd = 0.5;
		materialKs = 0.5;
	}

	// Autobuz
	{
		pozitie_autobuz = glm::vec3(0, 0.5, 0);
		viteza_autobuz = 0;
		unghi_autobuz = 0;
	}
}

void Laborator7::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);	
}

// Primeste pozitia de start a portiunii de autostrada, cate portiuni sa creeze si
// returneaza pozitia de final a portiunii create
glm::vec2 Laborator7::autostrada_inainte(glm::vec2 start, int portiuni) {
	float pozitie_portiune_autostrada = start.x;
	float pozitie_stanga_dreapta = start.y;
	for (int i = 0; i < portiuni; i++) {
		pozitie_portiune_autostrada += 0.5;

		glm::mat4 bucataAutostrada = glm::mat4(1);
		bucataAutostrada = glm::translate(bucataAutostrada, glm::vec3(pozitie_stanga_dreapta, 0.51, pozitie_portiune_autostrada));
		bucataAutostrada = glm::scale(bucataAutostrada, glm::vec3(0.04, 10, 0.01));
		RenderSimpleMesh(meshes["plane"], shaders["ShaderLab7"], bucataAutostrada, glm::vec3(0.33));

		glm::mat4 borduraStanga = glm::mat4(1);
		borduraStanga = glm::translate(borduraStanga, glm::vec3(pozitie_stanga_dreapta - 1, 0.52, pozitie_portiune_autostrada));
		borduraStanga = glm::scale(borduraStanga, glm::vec3(0.1, 0.2, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], borduraStanga, glm::vec3(0.2));

		glm::mat4 borduraDreapta = glm::mat4(1);
		borduraDreapta = glm::translate(borduraDreapta, glm::vec3(pozitie_stanga_dreapta + 1, 0.52, pozitie_portiune_autostrada));
		borduraDreapta = glm::scale(borduraDreapta, glm::vec3(0.1, 0.2, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], borduraDreapta, glm::vec3(0.2));
	}

	glm::mat4 obstacol = glm::mat4(1);
	obstacol = glm::translate(obstacol, glm::vec3(start.y + 0.7, 0.52, start.x + portiuni * 0.5 / 3));
	obstacol = glm::scale(obstacol, glm::vec3(0.2));
	RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], obstacol, glm::vec3(0.33, 0.5, 0.75));

	obstacol = glm::mat4(1);
	obstacol = glm::translate(obstacol, glm::vec3(start.y - 0.7 , 0.52, start.x + portiuni * 0.5 / 6));
	obstacol = glm::scale(obstacol, glm::vec3(0.2));
	RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], obstacol, glm::vec3(0.33, 0.5, 0.75));

	return glm::vec2(pozitie_portiune_autostrada, pozitie_stanga_dreapta);
}

glm::vec2 Laborator7::autostrada_stanga(glm::vec2 start, int portiuni) {
	float pozitie_portiune_autostrada = start.x;
	float pozitie_stanga_dreapta = start.y;
	for (int i = 0; i < portiuni*5; i++) {
		pozitie_portiune_autostrada += 0.05;
		if (i % 2 == 0) {
			pozitie_stanga_dreapta += 0.05;
		}
		
		glm::mat4 bucataAutostrada = glm::mat4(1);
		bucataAutostrada = glm::translate(bucataAutostrada, glm::vec3(pozitie_stanga_dreapta, 0.51, pozitie_portiune_autostrada));
		bucataAutostrada = glm::scale(bucataAutostrada, glm::vec3(0.04, 10, 0.01));
		RenderSimpleMesh(meshes["plane"], shaders["ShaderLab7"], bucataAutostrada, glm::vec3(0.33));

		glm::mat4 borduraStanga = glm::mat4(1);
		borduraStanga = glm::translate(borduraStanga, glm::vec3(pozitie_stanga_dreapta - 1, 0.52, pozitie_portiune_autostrada));
		borduraStanga = glm::scale(borduraStanga, glm::vec3(0.1, 0.2, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], borduraStanga, glm::vec3(0.2));

		glm::mat4 borduraDreapta = glm::mat4(1);
		borduraDreapta = glm::translate(borduraDreapta, glm::vec3(pozitie_stanga_dreapta + 1, 0.52, pozitie_portiune_autostrada));
		borduraDreapta = glm::scale(borduraDreapta, glm::vec3(0.1, 0.2, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], borduraDreapta, glm::vec3(0.2));
	}
	return glm::vec2(pozitie_portiune_autostrada, pozitie_stanga_dreapta);
}

glm::vec2 Laborator7::autostrada_dreapta(glm::vec2 start, int portiuni) {
	float pozitie_portiune_autostrada = start.x;
	float pozitie_stanga_dreapta = start.y;

	for (int i = 0; i < portiuni * 5; i++) {
		pozitie_portiune_autostrada += 0.05;
		if (i % 2 == 0) {
			pozitie_stanga_dreapta -= 0.05;
		}

		glm::mat4 bucataAutostrada = glm::mat4(1);
		bucataAutostrada = glm::translate(bucataAutostrada, glm::vec3(pozitie_stanga_dreapta, 0.51, pozitie_portiune_autostrada));
		bucataAutostrada = glm::scale(bucataAutostrada, glm::vec3(0.04, 10, 0.01));
		RenderSimpleMesh(meshes["plane"], shaders["ShaderLab7"], bucataAutostrada, glm::vec3(0.33));

		glm::mat4 borduraStanga = glm::mat4(1);
		borduraStanga = glm::translate(borduraStanga, glm::vec3(pozitie_stanga_dreapta - 1, 0.52, pozitie_portiune_autostrada));
		borduraStanga = glm::scale(borduraStanga, glm::vec3(0.1, 0.2, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], borduraStanga, glm::vec3(0.2));

		glm::mat4 borduraDreapta = glm::mat4(1);
		borduraDreapta = glm::translate(borduraDreapta, glm::vec3(pozitie_stanga_dreapta + 1, 0.52, pozitie_portiune_autostrada));
		borduraDreapta = glm::scale(borduraDreapta, glm::vec3(0.1, 0.2, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], borduraDreapta, glm::vec3(0.2));
	}
	return glm::vec2(pozitie_portiune_autostrada, pozitie_stanga_dreapta);
}

void Laborator7::reset_player() {
	cout << "Scor : " << pozitie_autobuz.z << endl;

	unghi_autobuz = 0;
	viteza_autobuz = 0;
	pozitie_autobuz = glm::vec3(0, 0.5, 0);

	roata_fata_stanga = glm::vec3(0.075, 0.5 - 0.017, 0.225);
	roata_fata_dreapta = glm::vec3(-0.14, 0.5 - 0.017, 0.225);
	roata_spate_stanga = glm::vec3(0.075, 0.5 - 0.017, -0.125);
	roata_spate_dreapta = glm::vec3(-0.14, 0.5 - 0.017, -0.125);

	GetSceneCamera()->SetRotation(glm::quat(RADIANS(0), glm::vec3(0, 1, 0)));

	lives--;
	if (lives == 0) {
		end_game = true;
	}
}

void Laborator7::Update(float deltaTimeSeconds)
{
	if (end_game == true) {
		return;
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		//modelMatrix = glm::translate(modelMatrix, glm::vec3(-2, 0.5f, 0));
		//SmodelMatrix = glm::rotate(modelMatrix, RADIANS(60.0f), glm::vec3(1, 1, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(230));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 0.5, 0.8));
	}

	// Render ground
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 0.5, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(100));
		RenderSimpleMesh(meshes["plane"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0,0.5,0));
	}

	// Render autostrada
	{
		float pozitie_portiune_autostrada = 0;
		float pozitie_stanga_dreapta = 0;
		glm::vec2 pozitie_curenta(pozitie_portiune_autostrada, pozitie_stanga_dreapta);
		while (pozitie_curenta.x - pozitie_autobuz.z < 10) {
			for (auto p : autostrada) {
				switch (p.first) {
					case 1:
						pozitie_curenta = autostrada_inainte(pozitie_curenta, p.second);
						break;
					case 2:
						pozitie_curenta = autostrada_stanga(pozitie_curenta, p.second);
						break;
					case 3:
						pozitie_curenta = autostrada_dreapta(pozitie_curenta, p.second);
						break;
				}
				if (pozitie_curenta.x - pozitie_autobuz.z >= 10) {
					break;
				}
			}
		}
	}

	// Render autobuz
	{
		pozitie_autobuz.z += viteza_autobuz * cos(RADIANS(unghi_autobuz));
		pozitie_autobuz.x += viteza_autobuz * sin(RADIANS(unghi_autobuz));

		glm::mat4 autobuz = glm::mat4(1);
		
		autobuz = glm::translate(autobuz, pozitie_autobuz);
		autobuz = glm::scale(autobuz, glm::vec3(0.035, 0.035, 0.035));
		autobuz = glm::rotate(autobuz, RADIANS(unghi_autobuz), glm::vec3(0, 1, 0));
		
		RenderSimpleMesh(meshes["bus"], shaders["ShaderLab7"], autobuz, glm::vec3(0.5, 0.75, 0));
	}

	// Render roti autobuz
	{
		roata_fata_dreapta.z = pozitie_autobuz.z + 0.225 * cos(RADIANS(unghi_autobuz));
		roata_fata_dreapta.x = pozitie_autobuz.x -  0.14 * cos(RADIANS(unghi_autobuz));

		roata_fata_stanga.z = pozitie_autobuz.z + 0.225 * cos(RADIANS(unghi_autobuz));
		roata_fata_stanga.x = pozitie_autobuz.x + 0.075 * cos(RADIANS(unghi_autobuz));

		roata_spate_dreapta.z = pozitie_autobuz.z - 0.125 * cos(RADIANS(unghi_autobuz));
		roata_spate_dreapta.x = pozitie_autobuz.x -  0.14 * cos(RADIANS(unghi_autobuz));

		roata_spate_stanga.z = pozitie_autobuz.z - 0.125 * cos(RADIANS(unghi_autobuz));
		roata_spate_stanga.x = pozitie_autobuz.x + 0.075 * cos(RADIANS(unghi_autobuz));

		glm::mat4 roata = glm::mat4(1);

		// Roata spate stanga
		roata = glm::translate(roata, roata_spate_stanga);
		roata = glm::scale(roata, glm::vec3(0.0009));
		roata = glm::rotate(roata, RADIANS(unghi_autobuz), glm::vec3(0, 1, 0));
		roata = glm::rotate(roata, RADIANS(90), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["wheel"], shaders["ShaderLab7"], roata, glm::vec3(0.1));

		// Roata spate dreapta
		roata = glm::mat4(1);
		roata = glm::translate(roata, roata_spate_dreapta);
		roata = glm::scale(roata, glm::vec3(0.0009));
		roata = glm::rotate(roata, RADIANS(unghi_autobuz), glm::vec3(0, 1, 0));
		roata = glm::rotate(roata, RADIANS(90), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["wheel"], shaders["ShaderLab7"], roata, glm::vec3(0.1));

		// Roata fata stanga
		roata = glm::mat4(1);
		roata = glm::translate(roata, roata_fata_stanga);
		roata = glm::scale(roata, glm::vec3(0.0009));
		roata = glm::rotate(roata, RADIANS(unghi_autobuz), glm::vec3(0, 1, 0));
		roata = glm::rotate(roata, RADIANS(90), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["wheel"], shaders["ShaderLab7"], roata, glm::vec3(0.1));

		// Roata fata dreapta
		roata = glm::mat4(1);
		roata = glm::translate(roata, roata_fata_dreapta);
		roata = glm::scale(roata, glm::vec3(0.0009));
		roata = glm::rotate(roata, RADIANS(unghi_autobuz), glm::vec3(0, 1, 0));
		roata = glm::rotate(roata, RADIANS(90), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["wheel"], shaders["ShaderLab7"], roata, glm::vec3(0.1));
	}

	// Check collision
	{
		int contor = 1; // Cate bucati de autostrada am rendat pana acum
		bool flag = true;
		float pozitie_bucata_autostrada = 0;
		float z = 0;
		glm::vec3 pozitie_obstacol;
		while (flag) {
			for (auto p : autostrada) {
				if (pozitie_autobuz.z <= z) {
					flag = false;
					break;
				}
				switch (p.first) {
					case 1:
						// Coliziuni cu obstacolele
						pozitie_obstacol = glm::vec3(pozitie_bucata_autostrada + 0.7, 0.5, z + p.second * 0.5 / 3);

						if (distance(pozitie_autobuz, pozitie_obstacol) < 0.2) {
							reset_player();
						}
						pozitie_obstacol = glm::vec3(pozitie_bucata_autostrada - 0.7, 0.52, z + p.second * 0.5 / 6);
						if (distance(pozitie_autobuz, pozitie_obstacol) < 0.2) {
							reset_player();
						}


						pozitie_bucata_autostrada += 0;
						z += (p.second * 0.5);

						break;
					case 2:
						pozitie_bucata_autostrada += min((double)(pozitie_autobuz.z - z) / 2, (double)(p.second / 2) / 2 * 0.5);
						z += (p.second / 2) * 0.5;
						break;
					case 3:
						pozitie_bucata_autostrada -= min((double)(pozitie_autobuz.z - z) / 2, (double)(p.second / 2) / 2 * 0.5);
						z += (p.second / 2) * 0.5;
						break;
				}
			}
		}

		// Coliziuni cu bordurile
		if (pozitie_autobuz.x + 0.1 > pozitie_bucata_autostrada + 1 - 0.1 ||
			pozitie_autobuz.x - 0.1 < pozitie_bucata_autostrada - 1 + 0.1) {
			reset_player();
		}
	}

	// Render the point light in the scene
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, lightPosition);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.001f));
		//RenderMesh(meshes["sphere"], shaders["Simple"], modelMatrix);
	}


	// Render Camera
	{
		if (camera_type != 0) {
			if (camera_type == 1) {
				pozitie_camera = pozitie_autobuz + glm::vec3(-1.5 * sin(RADIANS(unghi_autobuz)), 0.5, -1.5 * cos(RADIANS(unghi_autobuz)));
			}
			else {
				pozitie_camera = pozitie_autobuz + glm::vec3(-0.5 * sin(RADIANS(unghi_autobuz)), 0.25, 0.5 * cos(RADIANS(unghi_autobuz)));
			}

			GetSceneCamera()->SetPosition(pozitie_camera);
			GetSceneCamera()->SetRotation(glm::quat(RADIANS(-unghi_autobuz / 2), glm::vec3(0, 1, 0)));
		}
	}
}

void Laborator7::FrameEnd()
{
	DrawCoordinatSystem();
}

void Laborator7::RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 & modelMatrix, const glm::vec3 &color)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;
	
	glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Set shader uniforms for light & material properties
	// TODO: Set light position uniform
	int loc_light_position = glGetUniformLocation(shader->program, "light_position");
	glUniform3f(loc_light_position, lightPosition.x, lightPosition.y, lightPosition.z);

	// TODO: Set eye position (camera position) uniform
	glm::vec3 eye_position = GetSceneCamera()->transform->GetWorldPosition();
	int loc_eye_position = glGetUniformLocation(shader->program, "eye_position");
	glUniform3f(loc_eye_position, eye_position.x, eye_position.y, eye_position.z);

	// TODO: Set material property uniforms (shininess, kd, ks, object color) 
	int loc_kd_position = glGetUniformLocation(shader->program, "material_kd");
	glUniform1f(loc_kd_position, materialKd);

	int loc_ks_position = glGetUniformLocation(shader->program, "material_ks");
	glUniform1f(loc_ks_position, materialKs);

	int loc_shininess_position = glGetUniformLocation(shader->program, "material_shininess");
	glUniform1i(loc_shininess_position, materialShininess);

	int loc_color_position = glGetUniformLocation(shader->program, "object_color");
	glUniform3f(loc_color_position, color.x, color.y, color.z);

	int location_time = glGetUniformLocation(shader->program, "Time");
	float time = Engine::GetElapsedTime() - ((int)(Engine::GetElapsedTime() / 24)) *  24;
	glUniform1f(location_time, time);

	int location_hour = glGetUniformLocation(shader->program, "Hour");
	glUniform1i(location_hour, (int) time);

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	
	// Bind view matrix
	glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
	int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	// Bind projection matrix
	glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

// Documentation for the input functions can be found in: "/Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator7::OnInputUpdate(float deltaTime, int mods)
{
	float speed = 2;

	if (!window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		glm::vec3 up = glm::vec3(0, 1, 0);
		glm::vec3 right = GetSceneCamera()->transform->GetLocalOXVector();
		glm::vec3 forward = GetSceneCamera()->transform->GetLocalOZVector();
		forward = glm::normalize(glm::vec3(forward.x, 0, forward.z));

		// Control light position using on W, A, S, D, E, Q
		if (window->KeyHold(GLFW_KEY_W)) lightPosition -= forward * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_A)) lightPosition -= right * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_S)) lightPosition += forward * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_D)) lightPosition += right * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_E)) lightPosition += up * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_Q)) lightPosition -= up * deltaTime * speed;
	}

	float constanta_viteza = deltaTime / 500;

	if (window->KeyHold(GLFW_KEY_A)) {
		unghi_autobuz += viteza_autobuz * 10;
	}
	if (window->KeyHold(GLFW_KEY_D)) {
		unghi_autobuz -= viteza_autobuz *10;
	}
	if (window->KeyHold(GLFW_KEY_W)) {
		if (viteza_autobuz < 0) {
			viteza_autobuz += 2 * constanta_viteza;
		}
		if (viteza_autobuz < 0.04) {
			viteza_autobuz += 2 * constanta_viteza;
		}
	}
	else if (window->KeyHold(GLFW_KEY_S)) {
		if (viteza_autobuz > 0) {
			viteza_autobuz -= 2 * constanta_viteza;
		}
		if (viteza_autobuz > -0.025) {
			viteza_autobuz -= 2 * constanta_viteza;
		}
	}
	
	
	if (! window->KeyHold(GLFW_KEY_W) && viteza_autobuz > 0) {
		viteza_autobuz -= constanta_viteza;
	}

	if (!window->KeyHold(GLFW_KEY_S) && viteza_autobuz < 0) {
		viteza_autobuz += constanta_viteza;
	}
}

void Laborator7::OnKeyPress(int key, int mods)
{
	if (key == GLFW_KEY_SPACE)
	{
		switch (polygonMode)
		{
		case GL_POINT:
			polygonMode = GL_FILL;
			break;
		case GL_LINE:
			polygonMode = GL_POINT;
			break;
		default:
			polygonMode = GL_LINE;
			break;
		}
	}

	if (key == GLFW_KEY_F)
	{
		camera_type = 0;
	}

	if (key == GLFW_KEY_O)
	{
		camera_type = 1;
	}

	if (key == GLFW_KEY_P)
	{
		camera_type = 2;
	}
}

void Laborator7::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator7::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator7::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator7::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator7::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator7::OnWindowResize(int width, int height)
{
}
